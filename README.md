ics-ans-role-mongodb
===================

Ansible role to install mongodb.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

Basic config:
```yaml
mongodb_bindips:
  - "127.0.0.1"
mongodb_port: 27017

mongodb_repository_baseurl: https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/4.0/x86_64/
mongodb_repository_gpgcheck: true
mongodb_repository_gpgkey: https://www.mongodb.org/static/pgp/server-4.0.asc

mongodb_user_admin_name: mongo_superadmin
mongodb_user_admin_password: defaultPasswordPleaseChange
```

Replica set options:
```yaml
mongodb_replica_set_enable: true
mongodb_replica_set_name: replset0
mongodb_replica_set_ansible_group: mongoClusterGroupName
mongodb_replica_key: |  # openssl rand -base64 756
  PFzPxEa4eGJ7BZNWLkGLWjR+/6zWCMMoa7hyPcBAJcC0EpeF60nK/Z5VyUUnYdmb
  0Lje9jzpIOL1TzZf7WnQ3hqd6MhW3Gc/Igb5AehfsRWLR4xBh0kSXv0GiqJHMRqc
  y92PpNC77/9WqEuRWfgxjHT1EiQRGqJdfUOZCcOfQI6OM0iaZHAuVJLD1GD+uEod
  ogHRDcmp6oEcNaQR6HwZEOhZCnT4AnQARCViWc5LtWUIVn0CufN3qFr1XQ5bDhHD
  /iV6e4rLQ2cK7ylXX0AdGpR5+xfPmiNIoeXZMIKR6imVCBonC7gYBZNSnDvy6IZp
  qKbeGDEsc4OnIv/AuNHMEmoobPKJgT2z/1940MnE/yCmHTgYZiTkNnSPf3Rn7cD9
  MFF2gBD0XLHmiC000000000000000000000000000000efqQdmrP5xrbW27A4+8+
  sKxiD+289LXFIT00+++NOT+A+PRODUCTION+KEY+++00xFTtEMfHY/WiwbeaK9RX
  ehy3RGkI/wME5J000000000000000000000000000000AGbxWjI/Mw613pSt5UZJ
  BSw6pjvEhHJaoW5dAwzTPoF3txwE3oro0/oPF2OauuEBsK+jLWvY+vB9aw3f5g7L
  01zsGXCZfFXLgW559SP7ye7qCxI+uIkDzrorwAUJrJTjuPO0smh/sUB0t6jNcQNV
  ezu0VQoF+fidxVx7oKdAzrci63baKVJzUsWGUCg811PgimQWJjziFof4yPr5P4O8
  aPF/dVp9YUZMEc/0KjkHFXkLE1WbQ8LixtM6cyhvk56IohdlDtF8NOVWB9AMr9CS
  igy0FxmKTAS3k84oOaxv38RYuu9s++4lV3Ak8mtK9Obq7QW3dbNDPkX3c9b2+7o/
  OcsAS3ouF0j3nIIV4oZ9tV1IzlvZ5PA/dTSWLN5g+2r9uW+pW+lWQJS4i2lDWF8x
  tOfjH0J9NH9lvFIH7z/mDHWZtIzfmxUPXjUZLPnBfeuryQv5
```
Please note you need an odd number of mongod in a replicaset.
The first element of ```mongodb_bindips``` will be used as interface between the replicaset members.

The following variables contain sensitive data and should be vaulted:
```
mongodb_replica_key
mongodb_user_admin_name
mongodb_user_admin_password
```

Examples
----------------
The following example deploys two standalone instances of MongoDB and two replica set with three nodes.
As example, the following URIs can be used to access the deployed services:
```
mongodb://user:pass@lonely-mongo1:27017/db
mongodb://user:pass@lonely-mongo2:4040/db
mongodb://user:pass@mongo-rs1-a:27017,mongo-rs1-b:27017,mongo-rs1-c:27017/db?replicaSet=rs0
mongodb://user:pass@test-mongo-1:2727,test-mongo-2:2727,test-mongo-3:2727/db?replicaSet=grayreplset
```
For more info about MongoDB connection strings https://docs.mongodb.com/manual/reference/connection-string/

Inventory:
```INI
[mongos]
lonely-mongo1
lonely-mongo2 mongodb_port=4040

[mongos:children]
mongo-replicaset1
another-rs

[mongo-replicaset1]
mongo-rs1-a
mongo-rs1-b
mongo-rs1-c

[mongo-replicaset1:vars]
mongodb_replica_set_enable = true
mongodb_replica_set_name = rs0
mongodb_replica_set_ansible_group = mongo-replicaset1
mongodb_replica_key = "Please create a key"

[another-rs]
test-mongo-1
test-mongo-2
test-mongo-3

[another-rs:vars]
mongodb_replica_set_enable = true
mongodb_replica_set_name = grayreplset
mongodb_replica_set_ansible_group = another-rs
mongodb_replica_key = "LXFITl5t5Anc2Q559SP7ye7qCxI+5KA9NHFGqLy"
mongodb_user_admin_name = graylog
mongodb_user_admin_password = GraySuperSafePass
mongodb_port = 2727
```


Playbook:
```yaml
- name: Deploy mongodbs
  hosts: mongodbs
  become: true
  roles:
    - role: ics-ans-role-mongodb
  vars:
    mongodb_bindips:
      - "{{ ansible_default_ipv4.address }}"
      - "127.0.0.1"
  tasks:
    - name: Create a test user in all the masters or standalone
      mongodb_user:
        login_user: "{{ mongodb_user_admin_name }}"
        login_password: "{{ mongodb_user_admin_password }}"
        login_database: admin
        name: bob
        password: bobPass
        state: present
        roles: readWrite
        database: bobDatabase
        update_password: on_create
      when: >
        mongodb_is_slave is not defined
        or not mongodb_is_slave
```

License
-------

BSD 2-clause
