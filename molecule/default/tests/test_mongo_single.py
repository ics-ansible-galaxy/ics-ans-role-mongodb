import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('mongo-single')


def test_mongo_single(host):
    cmd = host.run("python /tmp/mongotest.py -u 'mongodb://bob:bobPass@localhost/bobDatabase' -c bobSongs")
    assert cmd.rc == 0
    assert "3" in cmd.stdout
    assert int(cmd.stdout) == 3
