import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('mongodbs')


def test_daemon_running(host):
    assert host.socket("tcp://127.0.0.1:27017")
    s = host.service("mongod")
    assert s.is_running
    assert s.is_enabled


def test_security(host):
    cmd = host.run("echo show dbs | mongo")
    assert cmd.rc == 1 or "admin" not in cmd.stdout
    is_master_cmd = host.run("mongo -u admin --quiet -p defaultPassword --eval 'db.isMaster()[\"ismaster\"]' admin")
    if "false" in is_master_cmd.stdout:
        return  # no need to test on a replica
    # assert "command listDatabases requires authentication" in cmd.stdout
    cmd2 = host.run("echo show dbs | mongo -u admin -p defaultPassword admin")
    assert cmd2.rc == 0
    assert "admin" in cmd2.stdout
    cmd3 = host.run("echo show dbs | mongo -u bob -p bobPass bobDatabase")
    assert cmd3.rc == 0
    assert "bye" in cmd3.stdout
    assert "exception: login failed" not in cmd3.stderr
    cmd4 = host.run("echo show dbs | mongo -u bob -p bobPassWRONG bobDatabase")
    assert cmd4.rc == 1
    assert "bye" not in cmd4.stdout
    assert "exception: login failed" in cmd4.stderr or "exception: connect failed" in cmd4.stderr
