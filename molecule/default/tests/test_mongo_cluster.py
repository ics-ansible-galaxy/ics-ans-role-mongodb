import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('mongodb-rs0')


def test_mongo_cluster(host):
    hvars = host.ansible.get_variables()
    hostname = hvars['inventory_hostname']
    cmd = host.run("python /tmp/mongotest.py -u 'mongodb://bob:bobPass@localhost/bobDatabase?replicaset=rs0' -c {}".format(hostname))
    assert cmd.rc == 0
    assert "3" in cmd.stdout
    assert int(cmd.stdout) == 3
