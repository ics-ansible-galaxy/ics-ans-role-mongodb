#!/usr/bin/python
import pymongo
import argparse

SEED_DATA = [
    {
        'decade': '1970s',
        'artist': 'Debby Boone',
        'song': 'You Light Up My Life',
        'weeksAtOne': 10
    },
    {
        'decade': '1980s',
        'artist': 'Olivia Newton-John',
        'song': 'Physical',
        'weeksAtOne': 10
    },
    {
        'decade': '1990s',
        'artist': 'Mariah Carey',
        'song': 'One Sweet Day',
        'weeksAtOne': 16
    }
]


def main(uri, collection):
    client = pymongo.MongoClient(uri)
    db = client.get_default_database()

    songs = db[collection]
    songs.insert_many(SEED_DATA)
    query = {'song': 'One Sweet Day'}
    songs.update(query, {'$set': {'artist': 'Mariah Carey ft. Boyz II Men'}})
    cursor = songs.find({'weeksAtOne': {'$gte': 10}}).sort('decade', 1)
    out = []
    for doc in cursor:
        out.append('In the %s, %s by %s topped the charts for %d straight weeks.' %
                   (doc['decade'], doc['song'], doc['artist'], doc['weeksAtOne']))
    print(len(out))
    db.drop_collection(collection)
    client.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-u", "--uri",
                        default="mongodb://bob:bobPass@localhost/bobDatabase",
                        help="Mongo uri")
    parser.add_argument('-c', "--collection", default="songs",
                        help='Name of the collection to use for the tests')
    parser.add_argument('-r', "--replicaSet",
                        help='Name of the replica set to use')

    args = parser.parse_args()
    main(args.uri, args.collection)
