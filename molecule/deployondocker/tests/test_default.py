import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('mongodbs')


def test_daemon_running(host):
    assert host.socket("tcp://127.0.0.1:27017")
    s = host.service("mongod")
    assert s.is_running
    assert s.is_enabled
